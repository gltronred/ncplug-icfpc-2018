#! /bin/bash

solver=$1

DATA=data
OUT=/tmp/out

mkdir -p "${OUT}"

function run {
    mode=$1
    shift
    files="$@"

    for i in $files; do
        echo "$mode$i"
        ${solver} ${DATA} ${OUT} ${mode} ${i}
    done
}

run a {001..186}
run d {001..186}
run r {001..115}

