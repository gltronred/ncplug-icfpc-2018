#! /bin/bash

PRIVATE=883d720236994d1f9534c6c0686f3b39
UPLOADER="$(dirname $0)/dropbox_uploader.sh"
RDIR="ncplug-icfpc-2018"

if [ -z $1 ]; then
    echo "Usage: $0 <path/to/dir/with/*.nbt>"
fi

pushd "$1"

submission="submission-$(date +%Y-%m-%d-%H-%M-%S-%N).zip"

find ./ -iname "F[ADR][0-9][0-9][0-9].nbt" \
    | xargs zip --encrypt --password "${PRIVATE}" "/tmp/${submission}"

popd

sha=$(shasum -a 256 "/tmp/${submission}" | sed "s/ .*//g;")

${UPLOADER} -p upload "/tmp/${submission}" "/${RDIR}/${submission}"

url=$(${UPLOADER} share "/${RDIR}/${submission}" | sed "s/.*https/https/;")

echo "${url} ${sha}"

curl -L \
     --data-urlencode action=submit \
     --data-urlencode privateID=${PRIVATE} \
     --data-urlencode submissionURL="${url}" \
     --data-urlencode submissionSHA="${sha}" \
     https://script.google.com/macros/s/AKfycbzQ7Etsj7NXCN5thGthCvApancl5vni5SFsb1UoKgZQwTzXlrH7/exec | \
  jq '.'

