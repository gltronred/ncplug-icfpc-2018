{-# LANGUAGE StrictData #-}
{-# LANGUAGE BangPatterns #-}

module Simulator where

import           Coord
import           Types
import           Utils

import           Data.Bool
import           Data.Foldable
import           Data.Function (on)
import qualified Data.List as L
import           Data.Map.Merge.Strict
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Maybe
import           Data.Set (Set)
import qualified Data.Set as S

grd :: e -> Bool -> Either e ()
grd e = bool (Left e) (pure ())

executeStep :: State -> Either String State
executeStep state@State { stateEnergy = energy
                        , stateHarmonics = harmonics
                        , stateMatrix = model@(Matrix r matrix)
                        , stateBots = bots
                        , stateTrace = Trace trace
                        } = do
  grd "State is not well-formed" $ isWellFormed state
  let botCount = S.size bots
      (commands, trace') = splitAt botCount trace
  grd "There are less commands than active bots" $ botCount < length commands
  let botList = S.toAscList bots
      assignments = zip botList commands
  -- we compute all updates
  updates <- traverse (uncurry $ executeCommand state) assignments
  let (delta,botUpdates) = unzip updates
  -- and try to apply them, if we fail - there were interfering commands, so we return error
  let mat = M.map Undisturbed matrix
  (deltaE, harmonics', mat') <- foldlM mergeUpdate (0, harmonics, mat) delta
  bots' <- checkBotUpdates $ zip botList botUpdates
  -- updating system state
  let
    -- before applying updates
    globalFieldEnergy = if harmonics == High
                        then 30 * r * r * r
                        else 3 * r * r * r
    activeBotsEnergy = 20 * botCount
    -- applying updates
    energy' = energy + globalFieldEnergy + activeBotsEnergy + deltaE
    clearDisturbance (Undisturbed v) = v
    clearDisturbance (Disturbed v) = v
    matrix' = M.map clearDisturbance mat'
  pure State { stateEnergy = energy'
             , stateHarmonics = harmonics'
             , stateMatrix = Matrix r matrix'
             , stateBots = bots'
             , stateTrace = Trace trace'
             }

data Update
  = Volatile
  | Set Voxel
  deriving (Eq,Show,Read)

data Updated
  = Undisturbed Voxel
  | Disturbed Voxel
  deriving (Eq,Show,Read)

data BotUpdate
  = Stay
  | Die
  | Move Coord
  | Fork (Bot,Bot)
  | FusionTo Seed
  | FusionFrom Seed
  deriving (Eq,Show,Read)

-- | Delta = (change in energy, is harmonics flipped, cells that are touched by nanobots)
-- We can fold deltas
type Delta = (Int, Bool, Map Coord Update)

-- | FullDelta = (Delta, updates in bot states)
-- Bot states' updating requires access to states of all bots (I'm too sleepy to think more)
type FullDelta = (Delta, BotUpdate)

-- | Essentially, State without bots and trace. Bots are updated elsewhere
type World = (Int, Harmonics, Map Coord Updated)

executeCommand :: State -> Bot -> Command -> Either String FullDelta
executeCommand state@State{ stateMatrix=matrix } bot@Bot{ botPos=c } = \case
  Halt -> do
    grd "Halt precondition failed" $
      c == Coord 0 0 0 && stateHarmonics state == Low && S.size (stateBots state) == 1
    pure ((0, False, M.singleton c Volatile), Die)
  Wait -> pure ((0, False, M.singleton c Volatile), Stay)
  Flip -> pure ((0, True, M.singleton c Volatile), Stay)
  SMove lid -> do
    let c' = c `shiftLong` lid
    grd "SMove to invalid coord" $ c' `inside` matrix
    let affected = between c c'
    grd "SMove through full" $ all ((==Void) . (getMatrix matrix M.!)) affected
    pure ((2*mlen lid, False, M.fromList $ map (,Volatile) affected), Move c')
  LMove sid1 sid2 -> do
    let c' = c `shiftShort` sid1
        c''= c'`shiftShort` sid2
    grd "LMove to invalid coord" $ c' `inside` matrix && c'' `inside` matrix
    let reg1 = between c c'
        reg2 = between c' c''
        affected = reg1 ++ reg2
    grd "LMove through full" $ all ((==Void) . (getMatrix matrix M.!)) affected
    pure ((2*(mlen sid1 + 2 + mlen sid2), False, M.fromList $ map (,Volatile) affected), Move c'')
  Fission nd m -> do
    let bids = botSeeds bot
    grd "Fission with empty seeds" $ not $ S.null bids
    let c' = c `shiftNear` nd
    grd "Fission to invalid coord" $ c' `inside` matrix
    grd "Fission to full" $ getMatrix matrix M.! c' == Void
    grd "Not enough seeds to Fission" $ S.size bids >= m+1
    let (lower, upper) = S.splitAt (m+1) bids
        (bid1, seeds') = S.deleteFindMin lower
        old = bot { botSeeds = upper }
        new = Bot { botBid = bid1, botPos = c', botSeeds = seeds'}
    pure ((24, False, M.fromList $ map (,Volatile) [c,c']), Fork (old,new))
  Fill nd -> do
    let c' = c `shiftNear` nd
    grd "Fill invalid coord" $ c' `inside` matrix
    let voxel = getMatrix matrix M.! c'
        void = voxel == Void
    pure ((bool 12 6 void, False, M.fromList [(c,Volatile), (c',Set Full)]), Stay)
  VoidC nd -> do
    let c' = c `shiftNear` nd
    grd "Fill invalid coord" $ c' `inside` matrix
    let voxel = getMatrix matrix M.! c'
        void = voxel == Void
    pure ((bool 3 (-12) void, False, M.fromList [(c,Volatile), (c',Set Void)]), Stay)
  FusionP nd -> do
    let sec = c `shiftNear` nd
        mbid = botBid <$> M.lookup sec botByCoord
    grd "FusionP on invalid coord" $ sec `inside` matrix
    grd "FusionP misses partner" $ isJust mbid
    let Just bid = mbid
    pure ((-24, False, M.singleton c Volatile), FusionFrom bid)
  FusionS nd -> do
    let prim = c `shiftNear` nd
        mbid = botBid <$> M.lookup prim botByCoord
    grd "FusionS on invalid coord" $ prim `inside` matrix
    grd "FusionS misses partner" $ isJust mbid
    let Just bid = mbid
    -- we accounted energy in FusionP
    pure ((0, False, M.singleton c Volatile), FusionTo bid)
  where botByCoord = M.fromList $ map (\b -> (botPos b, b)) $ S.toList $ stateBots state -- bot positions are different at start

mergeUpdate :: World
            -> Delta
            -> Either String World
mergeUpdate (e,f,m) (dE,dF,dM) = do
  m' <- mergeMatrix dM m
  let f' = bool swapHarmonics id dF f
  pure (e+dE, f', m')

swapHarmonics :: Harmonics -> Harmonics
swapHarmonics High = Low
swapHarmonics Low = High

mergeMatrix :: Map Coord Update -> Map Coord Updated -> Either String (Map Coord Updated)
mergeMatrix upd m = mergeA dropMissing preserveMissing matchedToVoxel upd m
  where matchedToVoxel = zipWithMaybeAMatched $ \_c u w -> case (u,w) of
          (Volatile, Undisturbed v) -> Right $ Just $ Disturbed v
          (Set v', Undisturbed _) -> Right $ Just $ Disturbed v'
          (_, Disturbed _) -> Left "Conflict occured"

checkBotUpdates :: [(Bot,BotUpdate)] -> Either String (Set Bot)
checkBotUpdates upd = do
  let from (b, FusionFrom id) = Just (botBid b, id)
      from _ = Nothing
      to (b, FusionTo id) = Just (id, botBid b)
      to _ = Nothing
      fromBots = S.fromList $ mapMaybe from upd
      toBots = S.fromList $ mapMaybe to upd
  grd "Fusion targets do not coincide" $ fromBots == toBots
  let oldBots = M.fromList $ map (\(b,_) -> (botBid b, b)) upd
      newBots = foldl' applyBotUpdate oldBots $ map (\(b,u) -> (botBid b, u))upd
  pure $ S.fromList $ M.elems newBots

applyBotUpdate :: Map Seed Bot -> (Seed, BotUpdate) -> Map Seed Bot
applyBotUpdate bots (bid, update) = case update of
  Stay -> bots
  Die -> M.delete bid bots
  Move c -> M.adjust (\bot -> bot {botPos = c}) bid bots
  Fork (b1,b2) -> M.insert (botBid b2) b2 $ M.insert bid b1 bots
  FusionFrom from -> let
    b1 = bots M.! bid
    b2 = bots M.! from
    bot = b1 { botSeeds = S.unions [botSeeds b1, S.singleton from, botSeeds b2]}
    in M.delete from $ M.insert bid bot bots
  FusionTo to -> bots

isWellFormed :: State -> Bool
isWellFormed state =
  let mat@(Matrix r m) = stateMatrix state in
  -- * If the harmonics is Low, then all Full voxels of the matrix are grounded.
  ((stateHarmonics state == High) || M.foldlWithKey' (\a c v -> if v == Full then a && isGrounded mat c else a) True m) &&
  -- * Each active nanobot has a different identifier.
  -- Should we check this? It's kinda true by construction.
  -- * The position of each active nanobot is distinct and is Void in the matrix.
  (let coords = S.map botPos $ stateBots state in S.size coords == S.size (stateBots state) && all (\c -> m M.! c == Void) coords)
  -- * The seeds of each active nanobot are disjoint.
  -- Should hold by construction.
  -- * The seeds of each active nanobot does not include the identifier of any active nanobot.
  -- Should hold by construction.


isAllGrounded :: Matrix -> Bool
isAllGrounded mat@(Matrix r m) = setFull == setGrounded
  where
    setFull = M.foldlWithKey' (\s c v -> if v == Full then S.insert c s else s) S.empty m
    setGrounded = go S.empty $ S.fromList $ filter filled [Coord x 0 z | x <- [0..r-1], z <- [0..r-1]] -- initial frontier is ground
    -- go precondition: frontier is inside the matrix and filled
    go !grounded !frontier
      | S.null frontier = grounded
      | otherwise       = go grounded' frontier'
      where
        grounded' = grounded `S.union` frontier
        frontier' = S.foldl' (\s c -> s `S.union` S.fromList (filter filled . filter (`S.notMember` grounded) $ newFrontier c)) S.empty frontier
    filled c = c `inside` mat && c `S.member` setFull
    newFrontier (Coord x y z) = Coord x (y+1) z : Coord x (y-1) z : beside
      where
        beside = [Coord (x-1) y z, Coord (x+1) y z, Coord x y (z-1), Coord x y (z+1)]

-- isGrounded :: Matrix -> Coord -> Bool
-- isGrounded mat@(Matrix _ m) c = filled c && (isJust $ bfs neighbours _y filled ((== 0) . _y) c)
--   where
--     neighbours (Coord x y z) = [Coord (x-1) y z, Coord (x+1) y z, Coord x (y-1) z, Coord x (y+1) z, Coord x y (z-1), Coord x y (z+1)]
--     filled c = c `inside` mat && m M.! c == Full

isGrounded :: Matrix -> Coord -> Bool
isGrounded mat@(Matrix _ m) c@(Coord x y z) = (not $ filled c) || y == 0 || any (isGrounded mat) (below : beside ++ above)
  where
    below  = Coord x (y-1) z
    beside = [Coord (x-1) y z, Coord (x+1) y z, Coord x y (z-1), Coord x y (z+1)]
    above  = [Coord x (y+1) z]
    filled c = c `inside` mat && m M.! c == Full

initialDisassemblyState :: Matrix -> Trace -> State
initialDisassemblyState src trace = State
  { stateEnergy = 0
  , stateHarmonics = Low
  , stateMatrix = src
  , stateBots = S.singleton $ Bot {botBid = 1, botPos = Coord 0 0 0, botSeeds = S.fromList [2..40]}
  , stateTrace = trace
  }

initialAssemblyState :: Int -> Trace -> State
initialAssemblyState r = initialDisassemblyState (Matrix r M.empty)

initialReassemblyState :: Matrix -> Trace -> State
initialReassemblyState = initialDisassemblyState

finalAssemblyState :: Matrix -> State -> Either String Int
finalAssemblyState tgt State { stateEnergy = e
                             , stateHarmonics = h
                             , stateMatrix = m
                             , stateBots = bots
                             , stateTrace = Trace left
                             } = do
  grd "Harmonics should be Low" $ h == Low
  grd "Target is incorrect" $ m == tgt
  grd "Bots are halted" $ S.null bots
  grd "Trace is fully consumed" $ null left
  pure e

finalDisassemblyState :: Int -> State -> Either String Int
finalDisassemblyState r = finalAssemblyState (Matrix r M.empty)

finalReassemblyState :: Matrix -> State -> Either String Int
finalReassemblyState = finalAssemblyState

simulate :: Monad m
         => (Int -> State -> m ())       -- ^ logging function
         -> (State -> Either String Int) -- ^ final energy computation
         -> State                        -- ^ initial state
         -> Int                          -- ^ step counter
         -> m (Either String Int)
simulate log final cur step = do
  log step cur
  let enext = executeStep cur
  case enext of
    Left e -> pure $ Left e
    Right next
      | null $ unTrace $ stateTrace next -> pure $ final next
      | otherwise -> simulate log final next $ step+1

