module Coord where

import           Data.Function (on)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M

import           Types
import           Utils


diff :: Coord -> Coord -> Coord
diff (Coord x1 y1 z1) (Coord x2 y2 z2) = Coord (x2-x1) (y2-y1) (z2-z1)

-- Coordinate shifts

shiftDiff :: Coord -> Coord -> Coord
shiftDiff (Coord x y z) (Coord dx dy dz) = Coord (x+dx) (y+dy) (z+dz)

shiftShort :: Coord -> ShortLinear -> Coord
shiftShort c (ShortLinear d) = c `shiftDiff` d

shiftLong :: Coord -> LongLinear -> Coord
shiftLong c (LongLinear d) = c `shiftDiff` d

shiftNear :: Coord -> NearCD -> Coord
shiftNear c (NearCD d) = c `shiftDiff` d

-- Test that coordinate is inside matrix
inside :: Coord -> Matrix -> Bool
inside (Coord x y z) (Matrix r _) = x`bw`r && y`bw`r && z`bw`r
  where bw c r = 0 <= c && c<r

between :: Coord -> Coord -> [Coord]
between (Coord x1 y1 z1) (Coord x2 y2 z2) = let
  minX = min x1 x2
  minY = min y1 y2
  minZ = min z1 z2
  maxX = max x1 x2
  maxY = max y1 y2
  maxZ = max z1 z2
  in [Coord x y z | x <- [minX .. maxX], y <- [minY .. maxY], z <- [minZ .. maxZ]]


-- This might've been moved to a separate @Model@ module

boundingBox2d :: Matrix -> Int -> Region
boundingBox2d (Matrix r m) y = M.foldrWithKey' bb (Coord r1 y r1, Coord 0 y 0) m
  where r1 = r-1
        bb _ Void reg = reg
        bb c Full reg@(mic, mac)
          | _y c == y = (mic `smaller` c, c `greater` mac)
          | otherwise = reg

boundingBox3d :: Matrix -> Region
boundingBox3d (Matrix r m) = M.foldrWithKey' bb (Coord r1 r1 r1, Coord 0 0 0) m
  where r1 = r-1
        bb _ Void reg = reg
        bb c Full (mic, mac) = (mic `smaller` c, c `greater` mac)

smaller, greater :: Coord -> Coord -> Coord
smaller (Coord x1 y1 z1) (Coord x2 y2 z2) = Coord (x1`min`x2) (y1`min`y2) (z1`min`z2)
greater (Coord x1 y1 z1) (Coord x2 y2 z2) = Coord (x1`max`x2) (y1`max`y2) (z1`max`z2)

extendBB :: Region -> Region -> Region
extendBB bb1 bb2 = (fst bb1 `smaller` fst bb2, snd bb1 `greater` snd bb2)

height :: Matrix -> Int
height = _y . snd . boundingBox3d

shortestPath :: Matrix -> Coord -> Coord -> Maybe [Coord]
shortestPath mat@(Matrix r m) start end = bfs neighbours closerToEnd isVoid (== end) start
  where
    neighbours (Coord x y z) = [Coord (x-1) y z, Coord (x+1) y z, Coord x (y-1) z, Coord x (y+1) z, Coord x y (z-1), Coord x y (z+1)]
    closerToEnd = mlen . (`diff` end)
    isVoid c = c `inside` mat && m M.! c == Void

pathToMoves :: [Coord] -> [Command]
pathToMoves cs = zipWith toSMove cs (tail cs)
  where
    toSMove (Coord x1 y1 z1) (Coord x2 y2 z2) = SMove $ LongLinear $ Coord (x2-x1) (y2-y1) (z2-z1)

smove :: Int -> Int -> Int -> Command
smove x y z = SMove $ LongLinear $ Coord x y z

smoveN :: Int -> Coord -> Command
smoveN m (Coord x y z) = smove (m*x) (m*y) (m*z)

contractSMoves :: [Command] -> [Command]
contractSMoves grp@(SMove (LongLinear dir) : _) = let
  cnt = length grp
  d = min 15 cnt
  (n,l) = cnt `divMod` d
  in (if l/=0 then (smoveN l dir :) else id) $ replicate n (smoveN d dir)
contractSMoves grp = grp

getLayer :: Map Coord Voxel -> Int -> Map Coord Voxel
getLayer m t = M.filterWithKey (\c _ -> (==t) $ _y c) m

