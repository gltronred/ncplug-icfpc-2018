module Solver.FlipOnlyWhenNecessary where

import           Coord
import           Simulator
import           Types

import           Data.Bool
import           Data.Function
import           Data.List hiding (map)
import           Data.Map.Strict (Map, empty, foldlWithKey', unionWithKey)
import qualified Data.Map.Strict as M
import           Data.Set (toList)

contractLines :: Solver
contractLines (Trace dft) _input = Trace $ concatMap contractSMoves $ group dft

flipOnlyWhenNecessary :: Solver
flipOnlyWhenNecessary dft = \case
  Assembly tgt -> optimize tgt (getYM (destroyed tgt) 1) dft
  Disassembly src -> optimize src (getYM src $ -1) dft
  Reassembly src tgt -> Trace $ concatMap contractSMoves $ group $ unTrace dft
  where destroyed (Matrix r m) = Matrix r $ M.map (const Void) m
        getYM m dir = (m, Coord 0 0 0, Low, dir)

optimize :: Matrix -> (Matrix,Coord,Harmonics,Int) -> Trace -> Trace
optimize model state (Trace t) = Trace $
                                 betterPathContraction $
                                 concatMap contractSMoves $ group $
                                 concat $ go state [] $ filter (/=Flip) t
  where
    go :: (Matrix,Coord,Harmonics,Int) -> [[Command]] -> [Command] -> [[Command]]
    go cur acc [] = reverse acc
    go cur@(_,cy,ch,dir) acc (cmd : cmds) = let
      (update, emitted) = emitNew model cur cmd
      in case update of
           Nothing -> go cur (emitted : acc) cmds
           Just (nm,ny,nh)
             | all isSMove cmds -> let
                 flip = if nh == High then [Flip] else []
               in go cur (cmds : emitted : flip : [Wait] : acc) [] -- we start to move to origin
             | otherwise ->
               go (nm,ny,nh,dir) (emitted : acc) cmds

emitNew :: Matrix
        -> (Matrix,Coord,Harmonics,Int)
        -> Command
        -> (Maybe (Matrix,Coord,Harmonics), [Command])
emitNew (Matrix r model)
  (curModel,old@(Coord _ oldY _),harm,dir)
  cmd@(SMove ld) =
  (Just (curModel, old `shiftLong` ld, harm), pure cmd)
emitNew (Matrix r model) (curModel, old@(Coord _ curY _), harm, dir) cmd
  | isChange cmd = let
      -- nextY = oldY
      -- curY = nextY-1
      -- curLayer = getLayer model curY
      prevY = curY-1
      -- prevLayer = getLayer model prevY
      -- groundedLevel = allUnder curLayer prevLayer prevY
      -- bottom = Matrix r $ getBottom model curY
      newModel = updateModel curModel old nd (change cmd)
      groundedLevel = all (isGrounded newModel) $ [Coord x prevY z | x <- [0..r-1], z <- [0..r-1]]
      -- wasFlipped = harm == High || not groundedLevel
      flipNeeded = (harm == Low) /= groundedLevel
      nd = getNearD cmd
    in (Just (newModel, old, if flipNeeded then flipHarm harm else harm),
        if flipNeeded
        then [Flip,cmd]
        else pure cmd)
  | otherwise = (Nothing, pure cmd)

isChange (Fill _) = True
isChange (VoidC _) = True
isChange _ = False

change (Fill _) = Full
change (VoidC _) = Void

getNearD (Fill nd) = nd
getNearD (VoidC nd) = nd

updateModel :: Matrix -> Coord -> NearCD -> Voxel -> Matrix
updateModel (Matrix r m) c n v = Matrix r $ M.insert (c `shiftNear` n) v m

flipHarm :: Harmonics -> Harmonics
flipHarm Low  = High
flipHarm High = Low

allUnder :: Map Coord Voxel -> Map Coord Voxel -> Int -> Bool
allUnder curLayer prevLayer prevY = prevY < 0 || M.foldrWithKey' helper True curLayer
  where helper (Coord x _ z) voxel res =
          res && (voxel == Void || prevLayer M.! Coord x prevY z == Full)

betterPathContraction :: [Command] -> [Command]
betterPathContraction = splittingOnWait $
                        concatMap betterContractSMoves . groupBy ((==) `on` onlyMoves)
  where
    onlyMoves :: Command -> Bool
    onlyMoves (SMove _) = True
    onlyMoves _ = False
    splittingOnWait :: ([Command] -> [Command]) -> [Command] -> [Command]
    splittingOnWait f cmds = let
      (before, Wait : after) = span (/=Wait) cmds
      in f before ++ after

betterContractSMoves :: [Command] -> [Command]
betterContractSMoves grp@(SMove _ : _) = let
  getShift (SMove ld) = ld
  getShift _ = LongLinear $ Coord 0 0 0
  Coord x y z = foldl' shiftLong (Coord 0 0 0) $ map getShift grp
  in concatMap contractSMoves $ group $
     nmove y (Coord 0 1 0) ++ nmove x (Coord 1 0 0) ++ nmove z (Coord 0 0 1)
betterContractSMoves grp = grp

nmove n dir@(Coord dx dy dz)
  | n > 0 = replicate n $ smove dx dy dz
  | n ==0 = []
  | n < 0 = replicate (-n) $ smove (-dx) (-dy) (-dz)

isSMove :: Command -> Bool
isSMove (SMove _) = True
isSMove Halt = True
isSMove _ = False

