module Solver.FillOnlyLayerBB where

import           Coord
import           Types

import           Data.List
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Maybe

fillOnlyLayerBB :: Solver
fillOnlyLayerBB _ = \case
  Assembly tgt -> uniformStrategy (destroyed tgt) tgt 1
  Disassembly src -> uniformStrategy src (destroyed src) (height src + 1)
  Reassembly src tgt -> uniformStrategy src tgt (height src `max` height tgt + 1)
  where destroyed (Matrix r m) = Matrix r $ M.map (const Void) m

matrixLayers :: Matrix -> Int -> [Region]
matrixLayers m maxY = map (boundingBox2d m) [0..maxY]

uniformStrategy :: Matrix -> Matrix -> Int -> Trace
uniformStrategy src tgt startY = let
  bb1 = boundingBox3d src
  bb2 = boundingBox3d tgt
  bb = extendBB bb1 bb2
  maxY = _y $ snd bb
  r1 = getResolution src - 1
  bbs = (if startY > 1 then reverse else id) $
        take maxY $
        zipWith extendBB
        (matrixLayers src (_y $ snd bb1))
        (matrixLayers tgt (_y $ snd bb2))
  layerNumbers = (if startY > 1 then reverse else id) $ zip [1..maxY] bbs
  in Trace $
     (contractSMoves $ replicate startY $ smove 0 1 0) ++
     [Flip] ++
     concat (reverse $ fst $ foldl' (strategyLevel src tgt) ([], Coord 0 startY 0) layerNumbers)

strategyLevel :: Matrix -> Matrix -> ([[Command]], Coord) -> (Int, Region) -> ([[Command]], Coord)
strategyLevel (Matrix r source) (Matrix _ target) (acc, start) (level, bb) = let
  src = getLayer source level
  tgt = getLayer target (level-1)
  cur = M.unionWithKey (\(Coord _ y _) s t -> if y < level-1 then s else t) source target
  pathToStart = contractSMoves $ pathToMoves $ fromJust $ shortestPath (Matrix r cur) start (fst bb)
  fillCommands = fillLevel src tgt (fst bb)
  in (fillCommands : pathToStart : acc, snd bb)

fillLevel :: Map Coord Voxel -> Map Coord Voxel -> Coord -> [Command]
fillLevel src tgt c = [Fill $ NearCD $ Coord 0 (-1) 0]

