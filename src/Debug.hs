module Debug where

import Types

import Data.List

class PP a where
  pp :: a -> String

instance PP Coord where
  pp (Coord x y z) = concat ["(", show x, ",", show y, ",", show z, ")"]
instance PP ShortLinear where
  pp (ShortLinear (Coord x y z)) = concat ["S<", show x, ",", show y, ",", show z, ">"]
instance PP LongLinear where
  pp (LongLinear (Coord x y z)) = concat ["L<", show x, ",", show y, ",", show z, ">"]
instance PP NearCD where
  pp (NearCD (Coord x y z)) = concat ["N<", show x, ",", show y, ",", show z, ">"]
instance PP FarCD where
  pp (FarCD (Coord x y z)) = concat ["F<", show x, ",", show y, ",", show z, ">"]

instance PP Command where
  pp = \case
    Halt -> "Halt"
    Wait -> "Wait"
    Flip -> "Flip"
    SMove ll -> "SMove " ++ pp ll
    LMove s1 s2 -> "LMove " ++ pp s1 ++ " " ++ pp s2
    Fill nd -> "Fill " ++ pp nd
    c -> show c

instance PP a => PP [a] where
  pp as = "[" ++ concat (intersperse ", " $ map pp as) ++ "]"

