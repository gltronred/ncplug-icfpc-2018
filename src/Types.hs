{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE StrictData #-}

module Types where

import Data.Map.Strict (Map)
import Data.Set (Set)
import GHC.Generics

data Coord = Coord { _x :: {-# UNPACK #-} Int, _y :: {-# UNPACK #-} Int, _z :: {-# UNPACK #-} Int } deriving (Eq,Ord,Show,Read,Generic)

-- | Manhattan distance
class Manhattan a where
  mlen :: a -> Int

-- | Chebyshev distance, also known as L_\infty norm
class Chebyshev a where
  clen :: a -> Int

instance Manhattan Coord where
  mlen (Coord dx dy dz) = abs dx + abs dy + abs dz
instance Chebyshev Coord where
  clen (Coord dx dy dz) = abs dx `max` abs dy `max` abs dz

newtype ShortLinear = ShortLinear { unShortLinear :: Coord }
  deriving (Eq,Show,Read,Generic, Manhattan)
newtype LongLinear = LongLinear { unLongLinear :: Coord }
  deriving (Eq,Show,Read,Generic, Manhattan)
newtype NearCD = NearCD { unNearCD :: Coord } deriving (Eq,Show,Read,Generic)
newtype FarCD = FarCD { unFarCD :: Coord } deriving (Eq,Show,Read,Generic)

-- isAdjacent

isLinear :: Coord -> Bool
isLinear (Coord dx dy dz) =
  dx /= 0 && dy == 0 && dz == 0 ||
  dx == 0 && dy /= 0 && dz == 0 ||
  dx == 0 && dy == 0 && dz /= 0

isShort :: Coord -> Bool
isShort c = mlen c <= 5

isLong :: Coord -> Bool
isLong c = mlen c <= 15

isNear :: Coord -> Bool
isNear d = 0 < mlen d && mlen d <= 2 && clen d == 1

isFar :: Coord -> Bool
isFar d = 0 < clen d && clen d <= 30

corrShortLinear :: ShortLinear -> Bool
corrShortLinear (ShortLinear d) = isLinear d && isShort d

corrLongLinear :: LongLinear -> Bool
corrLongLinear (LongLinear d) = isLinear d && isLong d

corrNearCD :: NearCD -> Bool
corrNearCD (NearCD d) = isNear d

corrFarCD :: FarCD -> Bool
corrFarCD (FarCD d) = isFar d

type Region = (Coord, Coord)

-- | Dimension of region
dim :: Region -> Int
dim (Coord x1 y1 z1, Coord x2 y2 z2) = x1`eq`x2 + y1`eq`y2 + z1`eq`z2
  where eq a b = if a==b then 0 else 1

data Voxel = Full | Void deriving (Eq,Show,Read,Generic)

data Matrix = Matrix
  { getResolution :: Int
  , getMatrix :: Map Coord Voxel
  } deriving (Eq,Show,Read,Generic)

data Harmonics = Low | High deriving (Eq,Show,Read,Generic)

type Seed = Int

data Bot = Bot
  { botBid :: Seed
  , botPos :: Coord
  , botSeeds :: Set Seed
  } deriving (Eq,Ord,Show,Read,Generic)
-- TODO: fix Ord instance, should compare only bid

data Command
  = Halt
  | Wait
  | Flip
  | SMove LongLinear
  | LMove ShortLinear ShortLinear
  | Fission NearCD Int
  | Fill NearCD
  | FusionP NearCD
  | FusionS NearCD
  | VoidC NearCD
  | GFill NearCD FarCD
  | GVoid NearCD FarCD
  deriving (Eq,Show,Read,Generic)

corrCommand :: Command -> Bool
corrCommand (SMove lld) = corrLongLinear lld
corrCommand (LMove s1 s2) = corrShortLinear s1 && corrShortLinear s2
corrCommand (Fission nd m) = corrNearCD nd && 0 <= m && m <= 40 -- 40 seeds max
corrCommand (Fill nd) = corrNearCD nd
corrCommand (FusionP nd) = corrNearCD nd
corrCommand (FusionS nd) = corrNearCD nd
corrCommand (VoidC nd) = corrNearCD nd
corrCommand (GFill nd fd) = corrNearCD nd && corrFarCD fd
corrCommand (GVoid nd fd) = corrNearCD nd && corrFarCD fd
corrCommand _ = True

newtype Trace = Trace { unTrace :: [Command] } deriving (Eq,Show,Read,Generic)

data State = State
  { stateEnergy :: Int
  , stateHarmonics :: Harmonics
  , stateMatrix :: Matrix
  , stateBots :: Set Bot
  , stateTrace :: Trace
  } deriving (Eq,Show,Read,Generic)

data Input
  = Assembly { tgt :: Matrix }
  | Disassembly { src :: Matrix }
  | Reassembly { src :: Matrix, tgt :: Matrix }
  deriving (Eq,Show,Read)

type LSolver = Matrix -> Trace -> Trace
type Solver = Trace -> Input -> Trace

