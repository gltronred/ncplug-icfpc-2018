{-# LANGUAGE StrictData #-}

module Lib
  ( mkLightning
  , mkFull
  , removeFlips
  , flipOnlyWhenNecessary
  , contractLines
  , fillOnlyLayerBB
  ) where

import Types
import Encode.Model
import Encode.Trace
import Solver.FlipOnlyWhenNecessary (flipOnlyWhenNecessary, contractLines)
import Solver.FillOnlyLayerBB (fillOnlyLayerBB)

import Data.Binary
import System.Environment

mkLightning :: LSolver -> IO ()
mkLightning solver = do
  args <- getArgs
  case args of
    [modelFile, traceFile, outFile] -> do
      model <- decodeFile modelFile
      trace <- decodeFile traceFile
      let out = solver model trace
      encodeFile outFile out
    _ -> putStrLn "Usage: solver model deftTrace out"

mkFull :: Solver -> IO ()
mkFull solver = do
  args <- getArgs
  case args of
    [datadir, outdir, "a", ident] -> withInput datadir outdir "A" ident solver
    [datadir, outdir, "d", ident] -> withInput datadir outdir "D" ident solver
    [datadir, outdir, "r", ident] -> withInput datadir outdir "R" ident solver
    _ -> usage

usage :: IO ()
usage = do
  putStrLn "Usage: solver <datadir> <outdir> a <id>"
  putStrLn "       solver <datadir> <outdir> d <id>"
  putStrLn "       solver <datadir> <outdir> r <id>"

withInput :: FilePath -> FilePath -> String -> String -> Solver -> IO ()
withInput datadir outdir mode ident solver = do
  let srcFile = datadir ++ "/" ++ "F" ++ mode ++ ident ++ "_src.mdl"
      tgtFile = datadir ++ "/" ++ "F" ++ mode ++ ident ++ "_tgt.mdl"
      traceName = "F" ++ mode ++ ident ++ ".nbt"
      trcFile = datadir ++ "/" ++ traceName
      outFile = outdir ++ "/" ++ traceName
      outputSol t i = encodeFile outFile $ solver t i
  withFile trcFile $ \trace ->
    case mode of
      "A" ->
        withFile tgtFile $ \tgt ->
        outputSol trace $ Assembly tgt
      "D" ->
        withFile srcFile $ \src ->
        outputSol trace $ Disassembly src
      "R" ->
        withFile srcFile $ \src ->
        withFile tgtFile $ \tgt ->
        outputSol trace $ Reassembly src tgt

withFile :: Binary a => FilePath -> (a -> IO ()) -> IO ()
withFile file action = decodeFile file >>= action

removeFlips :: LSolver
removeFlips _matrix (Trace trace) = Trace $ filter (/=Flip) trace

