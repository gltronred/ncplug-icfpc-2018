module Utils where

import           Data.List (foldl')
import           Data.Set hiding (filter, foldl', null, splitAt)
import qualified Data.Heap as H

import Types


chunks :: Int -> [a] -> [[a]]
chunks k xs | null xs = []
            | length xs < k = pure xs
            | otherwise = let
                (chunk, rest) = splitAt k xs
                in chunk : chunks k rest

coords :: Int -> [Coord]
coords r = [Coord x y z | x <- [0..r-1], y <- [0..r-1], z <- [0..r-1]]

box :: Int -> Int -> [Coord]
box n m = [Coord x y z | x <- [n..m-1], y <- [n..m-1], z <- [n..m-1]]

boxHull :: Int -> [Coord]
boxHull r = [Coord x y z | x <- [0..r-1], y <- [0..r-1], z <- [0..r-1], x == 0 || y == 0 || z == 0 || x == r-1 || y == r-1 || z == r-1]


bfs
  :: Ord state
  => (state -> [state]) -- ^ generator of new states
  -> (state -> Int)     -- ^ priority of a state, the smaller the better
  -> (state -> Bool)    -- ^ is it a valid state?
  -> (state -> Bool)    -- ^ is it the final state?
  -> state              -- ^ initial state
  -> Maybe [state]      -- ^ a path from initial state to the final one
bfs gen priority good final = go empty H.empty []
  where
    go visited toVisit path curr
      | final curr = Just $ reverse $ curr:path
      | otherwise  = case filter suitable $ gen curr of
        [] -> let toVisit' = H.filter (notVisited . fst . H.payload) toVisit in case H.null toVisit' of
          True  -> Nothing
          False -> let (next, path') = H.payload $ H.minimum toVisit' in go (curr `insert` visited) (H.deleteMin toVisit') path' next
        ss -> let
            toVisit' = foldl' (\h s -> H.Entry (priority s) (s, curr:path) `H.insert` h) (H.filter (notVisited . fst . H.payload) toVisit) ss
            (next, path') = H.payload $ H.minimum toVisit'
          in go (curr `insert` visited) (H.deleteMin toVisit') path' next
      where
        suitable s = good s && notVisited s
        notVisited s = s `notMember` visited
