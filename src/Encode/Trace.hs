module Encode.Trace where

import Data.Binary
import Data.Binary.Get

import Encode.Common
import Types

getMany :: Binary a => Get [a]
getMany = do
  eof <- isEmpty
  if eof
    then pure []
    else (:) <$> get <*> getMany
{-# INLINE getMany #-}

instance Binary Trace where
  put (Trace commands) = mapM_ put commands -- do not put length of the list
  get = Trace <$> getMany
