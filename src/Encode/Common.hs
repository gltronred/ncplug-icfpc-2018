module Encode.Common where

import Data.Binary
import Data.Bits
import Data.Word

import Types


fromNearCD :: NearCD -> Word8
fromNearCD (NearCD (Coord dx dy dz)) = fromIntegral $ (dx + 1)*9 + (dy + 1)*3 + (dz + 1)

toNearCD :: Word8 -> NearCD
toNearCD b = let
  dz = fromIntegral (b `mod` 3) - 1
  dy = fromIntegral (b `div` 3) `mod` 3 - 1
  dx = fromIntegral (b `div` 9) `mod` 3 - 1
  in NearCD $ Coord dx dy dz

fromFarCD :: FarCD -> (Word8, Word8, Word8)
fromFarCD (FarCD (Coord dx dy dz)) =
  (fromIntegral $ dx + 30, fromIntegral $ dy + 30, fromIntegral $ dz + 30)

toFarCD :: (Word8, Word8, Word8) -> FarCD
toFarCD (a,b,c) = FarCD $ Coord (fromIntegral a - 30) (fromIntegral b - 30) (fromIntegral c - 30)

fromShortLinear :: ShortLinear -> (Word8, Word8)
fromShortLinear (ShortLinear (Coord dx dy dz))
  | dx /= 0 = (1, fromIntegral dx + 5)
  | dy /= 0 = (2, fromIntegral dy + 5)
  | dz /= 0 = (3, fromIntegral dz + 5)

toShortLinear :: Word8 -> Word8 -> ShortLinear
toShortLinear 1 i = ShortLinear $ Coord (fromIntegral i - 5) 0 0
toShortLinear 2 i = ShortLinear $ Coord 0 (fromIntegral i - 5) 0
toShortLinear 3 i = ShortLinear $ Coord 0 0 (fromIntegral i - 5)

fromLongLinear :: LongLinear -> (Word8, Word8)
fromLongLinear (LongLinear (Coord dx dy dz))
  | dx /= 0 = (1, fromIntegral dx + 15)
  | dy /= 0 = (2, fromIntegral dy + 15)
  | dz /= 0 = (3, fromIntegral dz + 15)

toLongLinear :: Word8 -> Word8 -> LongLinear
toLongLinear 1 i = LongLinear $ Coord (fromIntegral i - 15) 0 0
toLongLinear 2 i = LongLinear $ Coord 0 (fromIntegral i - 15) 0
toLongLinear 3 i = LongLinear $ Coord 0 0 (fromIntegral i - 15)

decodeSMove :: Word8 -> Word8 -> Command
decodeSMove b i = let a = b `shiftR` 4 in SMove $ toLongLinear a i

decodeLMove :: Word8 -> Word8 -> Command
decodeLMove b1 b2 = let
  a2 = b1 `shiftR` 6
  a1 = b1 `shiftR` 4 .&. 3
  i2 = b2 `shiftR` 4
  i1 = b2 .&. 15
  in LMove (toShortLinear a1 i1) (toShortLinear a2 i2)

instance Binary Command where
  put Halt = put (255 :: Word8)
  put Wait = put (254 :: Word8)
  put Flip = put (253 :: Word8)
  put (SMove lld) = do
    let (a, i) = fromLongLinear lld
    put $ (a `shiftL` 4) .|. 4
    put i
  put (LMove sld1 sld2) = do
    let (a1, i1) = fromShortLinear sld1
        (a2, i2) = fromShortLinear sld2
    put $ (a2 `shiftL` 6) .|. (a1 `shiftL` 4) .|. 12
    put $ (i2 `shiftL` 4) .|. i1
  put (Fission (fromNearCD -> nd) m) = do
    put $ (nd `shiftL` 3) .|. 5
    put $ (fromIntegral m :: Word8)
  put (Fill (fromNearCD -> nd)) = put $ (nd `shiftL` 3) .|. 3
  put (FusionP (fromNearCD -> nd)) = put $ (nd `shiftL` 3) .|. 7
  put (FusionS (fromNearCD -> nd)) = put $ (nd `shiftL` 3) .|. 6
  -- full round
  put (VoidC (fromNearCD -> nd)) = put $ (nd `shiftL` 3) .|. 2
  put (GFill (fromNearCD -> nd) (fromFarCD -> (fx,fy,fz))) = do
    put $ (nd `shiftL` 3) .|. 1
    put fx
    put fy
    put fz
  put (GVoid (fromNearCD -> nd) (fromFarCD -> (fx,fy,fz))) = do
    put $ (nd `shiftL` 3) .|. 0
    put fx
    put fy
    put fz

  get = do
    b <- get :: Get Word8
    case b of
      255 -> pure Halt
      254 -> pure Wait
      253 -> pure Flip
      _   -> let
        b3 = b .&. 7
        longShortBit = b `testBit` 3
        in case b3 of
        7 -> pure $ FusionP $ toNearCD $ b `shiftR` 3
        6 -> pure $ FusionS $ toNearCD $ b `shiftR` 3
        5 -> Fission (toNearCD $ b `shiftR` 3) . fromIntegral <$> (get :: Get Word8)
        4 -> if longShortBit
             then decodeLMove b <$> get
             else decodeSMove b <$> get
        3 -> pure $ Fill $ toNearCD $ b `shiftR` 3
        2 -> pure $ VoidC $ toNearCD $ b `shiftR` 3
        1 -> do
          fd <- (,,) <$> getWord8 <*> getWord8 <*> getWord8
          pure $ GFill (toNearCD $ b `shiftR` 3) (toFarCD fd)
        0 -> do
          fd <- (,,) <$> getWord8 <*> getWord8 <*> getWord8
          pure $ GVoid (toNearCD $ b `shiftR` 3) (toFarCD fd)

