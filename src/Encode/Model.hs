module Encode.Model where

import           Control.Monad
import           Data.Binary
import           Data.Binary.Get
import           Data.Bits
import           Data.ByteString (index)
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Word

import           Types
import           Utils

instance Binary Matrix where
  put (Matrix r m) = do
    putWord8 $ fromIntegral r
    forM_ (chunks 8 $ coords r) $ \c8 -> do
      let voxels = take 8 $ map (m M.!) c8 ++ repeat Void
          byte = foldl' (\b v -> 2*b + (if v == Full then 1 else 0)) 0 voxels
      putWord8 byte
  get = do
    r <- fromIntegral <$> getWord8
    bytes <- getByteString $ ceiling $ fromIntegral (r*r*r) / 8
    let fromBit (Coord x y z) = let bitNo = x*r*r + y*r + z
                                    byteNo = bitNo `div` 8
                                    bitShift = 7 - bitNo `mod` 8
                                    byte = bytes `index` byteNo
                                    val = byte `testBit` bitShift
                                in if val then Full else Void
        m = M.fromList [(c, fromBit c) | c <- coords r]
    pure $ Matrix r m

