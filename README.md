# NCPLUG submission for ICFPC 2018

## Team

- Alexander Tchitchigin aka Gabriel
- Mansur Ziatdinov aka gltronred

## Build/run

Assuming that models and default traces are in `data` folder and you want to get resulting traces in `res` folder.

Build solution using stack:

```
stack install --local-bin-path=dist
```

Run `sol2` (it is the best solution we came up with) on every task:

```
dist/sol2 data res a 001
dist/sol2 data res a 002
# ...
dist/sol2 data res a 186

dist/sol2 data res d 001
dist/sol2 data res d 002
# ...
dist/sol2 data res d 186

dist/sol2 data res r 001
dist/sol2 data res r 002
# ...
dist/sol2 data res r 115
```

Here `a`, `d` and `r` are just these letters, `a` for assembly problem, `d` for disassembly and `r` for reassembly. The last argument is the number of a problem.

## Solution idea

We optimise default trace in the following way

- emit Flip only when necessary (i.e. when after issueing next command the model becomes ungrounded);
- merge equal SMove-s (like SMove <1,0,0>, SMove <1,0,0>, ...) into one or several SMove-s with larger step;
- merge several SMove-s into one moving first in Y direction, then in X, then in Z.

Second optimisation was the first and the best one. However, we were too slow to send this solution during Lightning round.

## Feedback

The contest organisation was great! Working scoreboard, feedback from organisers, etc.

Task was interesting, but two team members were not enough :(

Thank you!

