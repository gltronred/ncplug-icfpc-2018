{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Instances where

import           Test.SmallCheck.Series
import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.QuickCheck as QC
import           Test.Tasty.SmallCheck as SC

import           Control.Monad
import qualified Data.Map.Strict as M

import           Types
import           Utils


instance Monad m => Serial m Coord
instance Monad m => Serial m NearCD
instance Monad m => Serial m FarCD
instance Monad m => Serial m ShortLinear
instance Monad m => Serial m LongLinear

instance Arbitrary ShortLinear where
  arbitrary = ShortLinear <$> oneof [ Coord <$> choose (-5,5) <*> pure 0 <*> pure 0
                                    , Coord <$> pure 0 <*> choose (-5,5) <*> pure 0
                                    , Coord <$> pure 0 <*> pure 0 <*> choose (-5,5)
                                    ]
  shrink = shrinkNothing
instance Arbitrary LongLinear where
  arbitrary = LongLinear <$> oneof [ Coord <$> choose (-15,15) <*> pure 0 <*> pure 0
                                    , Coord <$> pure 0 <*> choose (-15,15) <*> pure 0
                                    , Coord <$> pure 0 <*> pure 0 <*> choose (-15,15)
                                    ]
  shrink = shrinkNothing

nearCoordGen :: Gen (Int,Int,Int)
nearCoordGen = (,,) <$> pure 0 <*> choose (-1,1) <*> choose (-1,1)

farCoordGen :: Gen (Int,Int,Int)
farCoordGen = ((,,) <$> choose (-30,30) <*> choose (-30,30) <*> choose (-30,30))
              `suchThat` (\(a,b,c) -> a/=0 || b/=0 || c/=0)

wrap :: Int -> (Int,Int,Int) -> Coord
wrap 0 (a,b,c) = Coord a b c
wrap 1 (a,b,c) = Coord c a b
wrap 2 (a,b,c) = Coord b c a

instance Arbitrary NearCD where
  shrink = shrinkNothing
  arbitrary = NearCD <$> oneof [ wrap 0 <$> nearCoordGen
                                , wrap 1 <$> nearCoordGen
                                , wrap 2 <$> nearCoordGen
                                ]
instance Arbitrary FarCD where
  shrink = shrinkNothing
  arbitrary = FarCD <$> oneof [ wrap 0 <$> farCoordGen
                              , wrap 1 <$> farCoordGen
                              , wrap 2 <$> farCoordGen
                              ]

instance Monad m => Serial m Command
instance Arbitrary Command where
  shrink = shrinkNothing
  arbitrary = oneof
    [ pure Halt
    , pure Wait
    , pure Flip
    , SMove <$> arbitrary
    , LMove <$> arbitrary <*> arbitrary
    , Fission <$> arbitrary <*> (arbitrary `suchThat` (>=0))
    , Fill <$> arbitrary
    , FusionP <$> arbitrary
    , FusionS <$> arbitrary
    , VoidC <$> arbitrary
    , GFill <$> arbitrary <*> arbitrary
    , GVoid <$> arbitrary <*> arbitrary
    ]

instance Arbitrary Voxel where
  arbitrary = oneof [pure Void, pure Full]
  shrink = shrinkNothing
instance Arbitrary Matrix where
  arbitrary = sized $ \size -> do
    let n = max size 1
    r <- choose (1,n)
    voxels <- forM (coords r) $ \c -> (c,) <$> arbitrary
    pure $ Matrix r $ M.fromList voxels
