module SimulatorSpec (simulatorTests) where

import           Test.SmallCheck.Series
import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.QuickCheck as QC
import           Test.Tasty.SmallCheck as SC

import qualified Data.Map.Strict as M

import           Simulator
import           Types
import           Utils


simulatorTests :: TestTree
simulatorTests = testGroup "Simulator"
  [ isAllGroundedTests
  , isGroundedTests
  ]

fullBox :: Int -> Matrix
fullBox n = Matrix n m
  where
    m = M.fromList $ map (, Full) $ coords n

hollowBox :: Int -> Matrix
hollowBox n = Matrix n m
  where
    m = M.fromList $ map (\c@(Coord x y z) -> (c, if x == 0 || y == 0 || z == 0 || x == n-1 || y == n-1 || z == n-1 then Full else Void)) $ coords n

isGroundedTests :: TestTree
isGroundedTests = testGroup "isGrounded"
  [ testCase "Full box should be grounded" $
    assertBool "1..30" $ all (\n -> let box = fullBox n in all (isGrounded box) (coords n)) [1..30]
  , testCase "Hollow box should be grounded" $
    assertBool "1..30" $ all (\n -> let hbox = hollowBox n in all (isGrounded hbox) (boxHull n)) [1..30]
  , testCase "Inside of a hollow box should NOT be grounded" $
    assertBool "1..30" $ all (\n -> let hbox = hollowBox n in all (not . isGrounded hbox) (box 1 $ n-1)) [1..30]
  ]

isAllGroundedTests :: TestTree
isAllGroundedTests = testGroup "isAllGrounded"
  [ testCase "Full box should be grounded" $
    assertBool "1..30" $ all (\n -> isAllGrounded $ fullBox n) [1..30]
  , testCase "Hollow box should be grounded" $
    assertBool "1..30" $ all (\n -> isAllGrounded $ hollowBox n) [1..30]
  ]
