{-# LANGUAGE OverloadedStrings #-}

module EncodeSpec (encoderTests) where

import           Test.SmallCheck.Series
import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.QuickCheck as QC
import           Test.Tasty.SmallCheck as SC

import           Control.Monad
import           Data.Binary
import           Data.ByteString.Lazy (pack)
import qualified Data.ByteString.Lazy as B
import qualified Data.Map.Strict as M

import           Encode.Common
import           Encode.Model
import           Encode.Trace
import           Types
import           Utils

import           Instances ()


encoderTests :: TestTree
encoderTests = testGroup "Encoder"
  [ coordDiffs
  , moves
  , trace
  , model
  ]

coordDiffs :: TestTree
coordDiffs = testGroup "Diffs"
  [ SC.testProperty "NearCD: to . from == id" $
    \nd -> corrNearCD nd SC.==> toNearCD (fromNearCD nd) == nd
  , localOption (QuickCheckTests 10000) $
    QC.testProperty "FarCD: to . from == id" $
    \fd -> corrFarCD fd QC.==> toFarCD (fromFarCD fd) == fd
  , localOption (SmallCheckDepth 7) $
    SC.testProperty "ShortLinear: to . from == id" $
    \sld -> corrShortLinear sld SC.==> uncurry toShortLinear (fromShortLinear sld) == sld
  , localOption (SmallCheckDepth 8) $
    SC.testProperty "LongLinear: to . from == id (SC)" $
    \lld -> corrLongLinear lld SC.==> uncurry toLongLinear (fromLongLinear lld) == lld
  , testCase "fromLong 15 0 0 == 01 11110" $ fromLongLinear (LongLinear $ Coord 15 0 0) @?= (1,30)
  , QC.testProperty "LongLinear: to . from == id (QC)" $
    \lld -> corrLongLinear lld QC.==> uncurry toLongLinear (fromLongLinear lld) == lld
  ]

moves :: TestTree
moves = testGroup "Moves"
  [ localOption (SmallCheckDepth 4) $
    SC.testProperty "decode . encode == id (SC)" $
    \m -> corrCommand m SC.==> case decodeOrFail @Command (encode m) of
      Right ("", _, r) -> r == m
      _               -> False
  , QC.testProperty "decode . encode == id (QC)" $
    \m -> corrCommand m QC.==> case decodeOrFail @Command (encode m) of
      Right ("", _, r) -> r == m
      _ -> False
  -- simple commands
  , testCase "encode Halt == 11111111" $ encode Halt @?= pack [0xff]
  , testCase "encode Wait == 11111110" $ encode Wait @?= pack [0xfe]
  , testCase "encode Flip == 11111101" $ encode Flip @?= pack [0xfd]
  -- smove
  , testCase "encode SMove 15 0 0 == 00010100 00011101" $
    encode (SMove $ LongLinear $ Coord 15 0 0) @?= pack [0x14, 0x1e]
  , testCase "encode SMove 0 0 -7 == 00110100 00001000" $
    encode (SMove $ LongLinear $ Coord 0 0 (-7)) @?= pack [0x34, 0x08]
  , QC.testProperty "SMove -> 2 bytes" $
    \lld -> corrLongLinear lld QC.==> B.length (encode $ SMove lld) == 2
  -- lmove
  , testCase "encode LMove (5,0,0) (0,-5,0) == 10011100 00001010" $
    encode (LMove (ShortLinear $ Coord 5 0 0) (ShortLinear $ Coord 0 (-5) 0)) @?= pack [0x9c, 0x0a]
  , QC.testProperty "LMove -> 2 bytes" $
    \s1 s2 -> corrShortLinear s1 && corrShortLinear s2 QC.==> B.length (encode $ LMove s1 s2) == 2
  -- fusion
  , testCase "encode FusionP (-1,1,0) == 00111111" $
    encode (FusionP $ NearCD $ Coord (-1) 1 0) @?= pack [0x3f]
  , testCase "encode FusionS (1,-1,0) == 10011110" $
    encode (FusionS $ NearCD $ Coord 1 (-1) 0) @?= pack [0x9e]
  -- fission
  , testCase "encode Fission (0,0,1) 5 == 01110101 00000101" $
    encode (Fission (NearCD $ Coord 0 0 1) 5) @?= pack [0x75, 0x05]
  , testCase "encode Fission (1,0,0) 0 == 10110101 00000000" $
    encode (Fission (NearCD $ Coord 1 0 0) 0) @?= pack [0xb5, 0x00]
  -- fill
  , testCase "encode Fill (0,-1,0) == 01010011" $
    encode (Fill $ NearCD $ Coord 0 (-1) 0) @?= pack [0x53]
  -- void
  , testCase "encode Void (1,0,1) == 1011 1010" $
    encode (VoidC $ NearCD $ Coord 1 0 1) @?= pack [0xba]
  -- gfill
  , testCase "encode GFill (0,-1,0) (10,-15,20) == 0101 0001 0010 1000 0000 1111 0011 0010" $
    encode (GFill (NearCD $ Coord 0 (-1) 0) (FarCD $ Coord 10 (-15) 20)) @?=
    pack [0x51,0x28,0x0f,0x32]
  -- gvoid
  , testCase "encode GVoid (1,0,0) (5,5,-5) == 1011 0000 0010 0011 0010 0011 0001 1001" $
    encode (GVoid (NearCD $ Coord 1 0 0) (FarCD $ Coord 5 5 (-5))) @?=
    pack [0xb0,0x23,0x23,0x19]
  -- other tests
  , testCase "encode Fission (1 0 0) 0 == 10110101 00000000" $
    encode (Fission (NearCD $ Coord 1 0 0) 0) @?= pack [0xb5, 0x00]
  , testCase "decode b500 == Fission (1,0,0) 0" $
    decodeOrFail (pack [0xb5, 0x00]) @?= Right ("", 2, Fission (NearCD $ Coord 1 0 0) 0)
  ]

trace :: TestTree
trace = testGroup "Trace"
  [ QC.testProperty "decode . encode == id" $
    \ms -> all corrCommand ms QC.==> case decodeOrFail @Trace (encode $ Trace ms) of
      Right ("", _, r) -> r == Trace ms
      _ -> False
  , testCase "decode [] == Trace []" $
    decodeOrFail (pack []) @?= Right ("", 0, Trace [])
  , testCase "decode [0xff] == Trace [Halt]" $
    decodeOrFail (pack [0xff]) @?= Right ("", 1, Trace [Halt])
  ]

model :: TestTree
model = testGroup "Model"
  [ localOption (QuickCheckMaxSize 5) $
    QC.testProperty "decode . encode == id" $
    \model -> case decodeOrFail @Matrix (encode model) of
      Right ("", _, r) -> r == model
      _ -> False
  , testCase "Matrix 2 [(0,0,1) -> Full] ==> 00000010 01000000" $
    encode (Matrix 2 $ M.fromList [(c, if c==Coord 0 0 1 then Full else Void) | c <- coords 2]) @?=
    pack [0x02,0x40]
  , testCase "0x0240 decodes to Matrix above" $
    getMatrix (decode $ pack [0x02, 0x40]) M.! Coord 0 0 1 @?= Full
  , testCase "Matrix 2 [(0,1,0) -> Full] ==> 00000010 00100000" $
    encode (Matrix 2 $ M.fromList [(c, if c==Coord 0 1 0 then Full else Void) | c <- coords 2]) @?=
    pack [0x02,0x20]
  , testCase "0x0220 decodes to Matrix above" $
    getMatrix (decode $ pack [0x02, 0x20]) M.! Coord 0 1 0 @?= Full
  , testCase "Matrix 1 [(0,0,0) -> Full] ==> 0000 0001 1000 0000" $
    encode (Matrix 1 $ M.singleton (Coord 0 0 0) Full) @?= pack [0x01,0x80]
  ]

