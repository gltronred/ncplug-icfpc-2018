{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Test.SmallCheck.Series
import           Test.Tasty
import           Test.Tasty.HUnit
import           Test.Tasty.QuickCheck as QC
import           Test.Tasty.SmallCheck as SC

import           Control.Monad
import           Data.Binary
import           Data.ByteString.Lazy (pack)
import qualified Data.ByteString.Lazy as B
import qualified Data.Map.Strict as M

import           Coord
import           Types
import           Utils

import           Instances ()
import           EncodeSpec
import           SimulatorSpec

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ encoderTests
  , coordTests
  , simulatorTests
  ]

-- t :: TestTree
-- t = testGroup "test"
--   [ SC.testProperty "Fermat" $
--     \x y z n -> (n :: Integer) >= 3 SC.==> x^n + y^n /= (z^n :: Integer)
--   ]

coordTests :: TestTree
coordTests = testGroup "Coord"
  [ bboxTests
  , pathTests
  ]

emptyMatrix :: Int -> M.Map Coord Voxel
emptyMatrix r = M.fromList . map (, Void) $ coords r

horizontalCross :: Int -> Int -> M.Map Coord Voxel
horizontalCross r y = M.fromList . map (, Full) $ [Coord x y (r `div` 2) | x <- [0..r-1]] ++ [Coord (r `div` 2) y z | z <- [0..r-1]]

pathTests :: TestTree
pathTests = testGroup "Shortest path"
  [ emptyMatrixTests
  , nonEmptyMatrixTests
  ]

emptyMatrixTests :: TestTree
emptyMatrixTests = testGroup "Empty matrix"
  [ localOption (SmallCheckDepth 20) $
    SC.testProperty "x-line" $
    \r d -> r > 0 && d >= 0 && d < r SC.==> let
      start = Coord 0 (r `div` 2) (r `div` 2)
      end   = Coord d (r `div` 2) (r `div` 2)
      in shortestPath (Matrix r $ emptyMatrix r) start end == (Just $ between start end)
  , localOption (SmallCheckDepth 20) $
    SC.testProperty "y-line" $
    \r d -> r > 0 && d >= 0 && d < r SC.==> let
      start = Coord (r `div` 2) 0 (r `div` 2)
      end   = Coord (r `div` 2) d (r `div` 2)
      in shortestPath (Matrix r $ emptyMatrix r) start end == (Just $ between start end)
  , localOption (SmallCheckDepth 20) $
    SC.testProperty "z-line" $
    \r d -> r > 0 && d >= 0 && d < r SC.==> let
      start = Coord (r `div` 2) (r `div` 2) 0
      end   = Coord (r `div` 2) (r `div` 2) d
      in shortestPath (Matrix r $ emptyMatrix r) start end == (Just $ between start end)
  , testCase "Horizontal L-line" $ let
      start = Coord 0 2 1
      end   = Coord 4 2 4
    in shortestPath (Matrix 5 $ emptyMatrix 5) start end @?= (Just [Coord 0 2 1, Coord 0 2 2, Coord 0 2 3, Coord 0 2 4, Coord 1 2 4, Coord 2 2 4, Coord 3 2 4, Coord 4 2 4])
  ]

nonEmptyMatrixTests :: TestTree
nonEmptyMatrixTests = testGroup "Nonempty matrix"
  [ testCase "Horizontal L-line" $ let
      start = Coord 0 2 1
      end   = Coord 4 2 4
    in shortestPath (Matrix 5 $ M.insert (Coord 0 2 3) Full $ emptyMatrix 5) start end @?= (Just [Coord 0 2 1, Coord 0 2 2, Coord 1 2 2, Coord 1 2 3, Coord 1 2 4, Coord 2 2 4, Coord 3 2 4, Coord 4 2 4])
  ]

bboxTests :: TestTree
bboxTests = testGroup "Bounding box"
  [ localOption (SmallCheckDepth 10) $
    SC.testProperty "Bounding box of a square" $
    \m n r y -> m > 0 && n > m && r >= n && y >= m && y < n SC.==> boundingBox2d (Matrix r $ M.union (M.fromList . map (, Full) $ box m n) (emptyMatrix r)) y == (Coord m y m, Coord (n-1) y (n-1))
  , localOption (SmallCheckDepth 20) $
    SC.testProperty "Bounding box of a cross" $
    \r y -> r > 0 && y >= 0 && y < r SC.==> boundingBox2d (Matrix r $ M.union (horizontalCross r y) (emptyMatrix r)) y == (Coord 0 y 0, Coord (r-1) y (r-1))
  ]
